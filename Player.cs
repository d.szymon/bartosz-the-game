﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BartoszTheGame
{
    public class Player : IGameParticle
    {
        public int HP { get; set; }
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int Size => DrawingInfo.TotalPlayerSize;
        public DateTime LastMovement { get; set; }
        public DateTime LastShot { get; set; }
        public int KilledEnemies { get; set; }
        
        public Player()
        {
            this.HP = 5;
            this.PositionX = 0;
            this.LastMovement = DateTime.MinValue;
            this.LastShot = DateTime.MinValue;
            this.KilledEnemies = 0;
        }

        public Player(int positionX, int positionY) : this()
        {
            this.PositionX = positionX;
            this.PositionY = positionY;
        }

        public void MoveLeft()
        {
            this.PositionX -= 1;
            this.LastMovement = DateTime.UtcNow;
        }

        public void MoveRight()
        {
            this.PositionX += 1;
            this.LastMovement = DateTime.UtcNow;
        }

        public void UpdateLastShot()
        {
            this.LastShot = DateTime.UtcNow;
        }

        public bool IsMovementAllowed => (DateTime.UtcNow - this.LastMovement).Milliseconds >= 5;
        public bool IsShootingAllowed => (DateTime.UtcNow - this.LastShot).Milliseconds >= 300;
    }
}
