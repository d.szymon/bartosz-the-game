﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BartoszTheGame
{
    public interface IGameParticle 
    {
        int PositionX { get; set; }
        int PositionY { get; set; }
        int Size { get; }
    }
}
