﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BartoszTheGame
{
    public static class DrawingInfo
    {
        public const int PlayerSize = 10;
        public const int MarginBottom = 70;
        public const int PlayerSizeMultiplier = 2;
        public static int TotalPlayerSize = PlayerSize * PlayerSizeMultiplier;
        public const int DamageParticleSize = 10;
        public const int EnemySize = 20;
    }
}
