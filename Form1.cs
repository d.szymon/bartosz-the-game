﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;

namespace BartoszTheGame
{
    public partial class Form1 : Form
    {
        private readonly CancellationTokenSource gameTokenSource = new CancellationTokenSource();
        private readonly CancellationToken gameToken;
        private readonly PictureBox drawArea;
        private Player player;
        private List<DamageParticle> damageParticles = new List<DamageParticle>();
        private List<Enemy> enemies = new List<Enemy>();
        private DateTime lastEnemySpawn = DateTime.UtcNow;
        private readonly Random random = new Random();
        private readonly Font statsFont = new Font("Segoe UI", 8);

        public Form1()
        {
            InitializeComponent();
            this.gameToken = this.gameTokenSource.Token;
            this.Load += Form1_Load;
            this.drawArea = new PictureBox {Dock = DockStyle.Fill, BackColor = Color.Black};
            this.drawArea.Paint += DrawArea_Paint;
            this.Controls.Add(this.drawArea);
            this.player = new Player(this.Width / 2, this.Height - DrawingInfo.MarginBottom);
            this.Closing += Form1_Closing;
        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
            this.gameTokenSource.Cancel();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Task.Factory.StartNew(this.DrawThread, gameToken);
            var thread = new Thread(this.GameThread);
            thread.SetApartmentState(ApartmentState.STA);
            thread.Start();
        }

        private void GameThread()
        {
            while (!this.gameToken.IsCancellationRequested)
            {
                if (this.player.IsMovementAllowed)
                {
                    if (Keyboard.IsKeyDown(Key.Left) && this.player.PositionX > DrawingInfo.TotalPlayerSize)
                    {
                        this.player.MoveLeft();
                    }
                    else if (Keyboard.IsKeyDown(Key.Right) && this.player.PositionX < this.Width - DrawingInfo.TotalPlayerSize * 2)
                    {
                        this.player.MoveRight();
                    }
                }

                if (this.player.IsShootingAllowed && Keyboard.IsKeyDown(Key.Space))
                {
                    this.damageParticles.Add(new DamageParticle(this.player));
                    this.player.UpdateLastShot();
                }

                lock (this.damageParticles)
                {
                    foreach (var particle in this.damageParticles.Where(particle => particle.ShouldMove))
                        particle.Move();
                }

                this.damageParticles = this.damageParticles.Where(particle => !particle.ShouldBeRemoved).ToList();

                if ((DateTime.UtcNow - this.lastEnemySpawn).Seconds >= 2)
                {
                    this.enemies.Add(new Enemy(this.random.Next(DrawingInfo.EnemySize, this.Width - DrawingInfo.EnemySize * 2)));
                    this.lastEnemySpawn = DateTime.UtcNow;
                }
                
                var enemiesToRemove = new List<Enemy>();
                var particlesToRemove = new List<DamageParticle>();

                lock (this.enemies)
                {
                    foreach (var enemy in this.enemies.Where(enemy => enemy.ShouldMove))
                        enemy.Move();

                    lock (this.damageParticles)
                    {
                        foreach (var enemy in this.enemies)
                        {
                            foreach (var damageParticle in this.damageParticles.Where(damageParticle => enemy.IsColliding(damageParticle)))
                            {
                                enemiesToRemove.Add(enemy);
                                particlesToRemove.Add(damageParticle);
                                this.player.KilledEnemies++;
                            }

                            if (enemy.IsColliding(this.player))
                            {
                                this.player.HP = 0;
                            }
                        }
                    }
                }

                this.enemies = this.enemies.Where(enemy => !enemiesToRemove.Contains(enemy)).ToList();
                this.damageParticles = this.damageParticles.Where(particle => !particlesToRemove.Contains(particle)).ToList();

                lock (this.enemies)
                {
                    enemiesToRemove = this.enemies.Where(enemy => enemy.PositionY >= this.Height + 5).ToList();
                    this.player.HP = Math.Max(0, this.player.HP - enemiesToRemove.Count);
                }

                this.enemies = this.enemies.Where(enemy => !enemiesToRemove.Contains(enemy)).ToList();

                if (this.player.HP <= 0)
                {
                    MessageBox.Show("Przegrałeś!", "n00b", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                    break;
                }

                Thread.Sleep(TimeSpan.FromMilliseconds(1));
            }
        }

        private void DrawThread()
        {
            while (!this.gameToken.IsCancellationRequested)
            {
                if (this.drawArea.InvokeRequired)
                    this.drawArea.Invoke((MethodInvoker)delegate { this.drawArea.Refresh(); });
                else
                    this.drawArea.Refresh();

                Task.Delay(TimeSpan.FromMilliseconds(5), gameToken);
            }
        }

        private void DrawArea_Paint(object sender, PaintEventArgs e)
        {
            e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
            this.DrawPlayer(e.Graphics);
            this.DrawDamage(e.Graphics);
            this.DrawEnemies(e.Graphics);
            this.DrawStats(e.Graphics);
        }

        private void DrawPlayer(Graphics g)
        {
            if (this.player == null)
                return;

            g.FillEllipse(
                Brushes.White,
                this.player.PositionX - this.player.Size / 2,
                this.player.PositionY - this.player.Size / 2,
                this.player.Size,
                this.player.Size
            );
        }

        private void DrawDamage(Graphics g)
        {
            if (this.damageParticles.Count == 0)
                return;

            var particles = new List<DamageParticle>(this.damageParticles);

            foreach (var particle in particles)
            {
                g.FillEllipse(
                    Brushes.Red,
                    particle.PositionX - particle.Size / 2,
                    particle.PositionY - particle.Size / 2,
                    particle.Size,
                    particle.Size
                );
            }
        }

        private void DrawEnemies(Graphics g)
        {
            if (this.enemies.Count == 0)
                return;

            var enemies = new List<Enemy>(this.enemies);

            foreach (var enemy in enemies)
            {
                g.FillEllipse(
                    Brushes.DeepPink,
                    enemy.PositionX - enemy.Size/2,
                    enemy.PositionY - enemy.Size/2,
                    enemy.Size,
                    enemy.Size
                );
            }
        }

        private void DrawStats(Graphics g)
        {
            var statsX = 10;
            var nextY = 10;

            var hpStat = $"HP: {this.player.HP}";
            g.DrawString(hpStat, this.statsFont, Brushes.White, statsX, nextY);
            var lastSize = g.MeasureString(hpStat, this.statsFont);
            nextY += (int)lastSize.Height + 5;

            var killedStat = $"Killed: {this.player.KilledEnemies}";
            g.DrawString(killedStat, this.statsFont, Brushes.White, statsX, nextY);
            lastSize = g.MeasureString(killedStat, this.statsFont);
            nextY += (int) lastSize.Height + 5;
        }
    }
}
