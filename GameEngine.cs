﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BartoszTheGame
{
    public static class GameEngine
    {
        public static bool IsColliding(IGameParticle obj1, IGameParticle obj2)
        {
            return new Position(obj1).IsColliding(obj2);
        }

        private class Position
        {
            public int X1 { get; }
            public int X2 { get; }
            public int Y1 { get; }
            public int Y2 { get; }

            public Position(IGameParticle particle)
            {
                this.X1 = particle.PositionX - particle.Size / 2;
                this.X2 = particle.PositionX + particle.Size / 2;
                this.Y1 = particle.PositionY - particle.Size / 2;
                this.Y2 = particle.PositionY + particle.Size / 2;
            }

            public bool IsColliding(Position pos)
            {
                return ((pos.X1 <= this.X2 && pos.X2 >= this.X1) || (pos.X2 >= this.X1 && pos.X1 <= this.X2)) && ((pos.Y1 <= this.Y2 && pos.Y2 >= this.Y2) || (pos.Y2 >= this.Y1 && pos.Y2 <= this.Y2));
            }

            public bool IsColliding(IGameParticle particle)
            {
                return this.IsColliding(new Position(particle));
            }
        }
    }
}
