﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BartoszTheGame
{
    public class Enemy : IGameParticle
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int Size => DrawingInfo.EnemySize;
        public DateTime LastMovement { get; set; }
        public bool ShouldMove => (DateTime.UtcNow - this.LastMovement).Milliseconds >= 20;

        public Enemy(int posX)
        {
            this.PositionX = posX;
            this.LastMovement = DateTime.UtcNow;
        }

        public Enemy(int posX, int posY) : this(posX)
        {
            this.PositionY = posY;
        }

        public void Move()
        {
            this.PositionY += 1;
            this.LastMovement = DateTime.UtcNow;
        }

        public bool IsColliding(IGameParticle particle)
        {
            return GameEngine.IsColliding(this, particle);
        }
    }
}
