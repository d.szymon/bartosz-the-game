﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace BartoszTheGame
{
    public class DamageParticle : IGameParticle
    {
        public int PositionX { get; set; }
        public int PositionY { get; set; }
        public int Size => DrawingInfo.DamageParticleSize;
        public DateTime LastMovement { get; set; }
        public bool ShouldMove => (DateTime.UtcNow - this.LastMovement).Milliseconds >= 5;
        public bool ShouldBeRemoved => this.PositionY < 0;

        public DamageParticle(int posX, int posY)
        {
            this.PositionX = posX;
            this.PositionY = posY;
            this.LastMovement = DateTime.UtcNow;
        }

        public DamageParticle(IGameParticle particle) : this(particle.PositionX, particle.PositionY)
        {

        }

        public void Move()
        {
            this.PositionY -= 1;
            this.LastMovement = DateTime.UtcNow;
        }

        public bool IsColliding(IGameParticle particle)
        {
            return GameEngine.IsColliding(this, particle);
        }
    }
}
